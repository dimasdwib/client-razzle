
const config = {
  api: {
    endpoint: process.env.RAZZLE_ENDPOINT || 'http://localhost/laravel-api/api/',
  }
};

export default config;
